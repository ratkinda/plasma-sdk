# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-21 01:57+0000\n"
"PO-Revision-Date: 2016-06-09 17:27-0600\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: contents/applet/AppletError.qml:40
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:48
msgid "Copy Error Details to Clipboard"
msgstr ""

#: contents/applet/CompactApplet.qml:74
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:20
#: contents/configuration/AppletConfiguration.qml:247
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:46
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:95
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:115
msgid "License:"
msgstr ""

#: contents/configuration/AboutPlugin.qml:128
msgid "Authors"
msgstr ""

#: contents/configuration/AboutPlugin.qml:138
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:149
msgid "Translators"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:55
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Keyboard Shortcuts"
msgstr "ਕੀਬੋਰਡ ਦੇ ਸ਼ਾਰਟਕੱਟ"

#: contents/configuration/AppletConfiguration.qml:294
msgid "Apply Settings"
msgstr "ਸੈਟਿੰਗਾਂ ਨੂੰ ਲਾਗੂ ਕਰੋ"

#: contents/configuration/AppletConfiguration.qml:297
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"ਮੌਜੂਦਾ ਮੋਡੀਊਲ ਦੀਆਂ ਸੈਟਿੰਗਾਂ ਨੂੰ ਬਦਲਿਆ ਗਿਆ ਹੈ। ਕੀ ਤੁਸੀਂ ਬਦਲਾਅ ਲਾਗੂ ਕਰਨੇ ਚਾਹੁੰਦੇ ਹੋ ਜਾਂ ਅਣਡਿੱਠੇ?"

#: contents/configuration/AppletConfiguration.qml:328
msgid "OK"
msgstr "ਠੀਕ ਹੈ"

#: contents/configuration/AppletConfiguration.qml:336
msgid "Apply"
msgstr "ਲਾਗੂ ਕਰੋ"

#: contents/configuration/AppletConfiguration.qml:342
msgid "Cancel"
msgstr "ਰੱਦ ਕਰੋ"

#: contents/configuration/ConfigCategoryDelegate.qml:28
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:19
msgid "Left-Button"
msgstr "ਖੱਬਾ-ਬਟਨ"

#: contents/configuration/ConfigurationContainmentActions.qml:20
msgid "Right-Button"
msgstr "ਸੱਜਾ-ਬਟਨ"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Middle-Button"
msgstr "ਮੱਧ ਬਟਨ"

#: contents/configuration/ConfigurationContainmentActions.qml:22
#, fuzzy
#| msgid "Left-Button"
msgid "Back-Button"
msgstr "ਖੱਬਾ-ਬਟਨ"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Forward-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Vertical-Scroll"
msgstr "ਵਰਟੀਕਲ-ਸਕਰੋਲ"

#: contents/configuration/ConfigurationContainmentActions.qml:26
msgid "Horizontal-Scroll"
msgstr "ਹਰੀਜੱਟਲ-ਸਕਰੋਲ"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:29
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Meta"
msgstr "ਮੇਟਾ"

#: contents/configuration/ConfigurationContainmentActions.qml:73
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:153
#: contents/configuration/MouseEventInputButton.qml:14
msgid "Add Action"
msgstr "ਕਾਰਵਾਈ ਨੂੰ ਜੋੜੋ"

#: contents/configuration/ConfigurationContainmentAppearance.qml:26
msgid "Appearance"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:68
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:83
msgid "Layout:"
msgstr "ਲੇਆਉਟ:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
msgid "Wallpaper Type:"
msgstr "ਵਾਲਪੇਪਰ ਦੀ ਕਿਸਮ"

#: contents/configuration/ConfigurationContainmentAppearance.qml:115
msgid "Get New Plugins..."
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Layout changes must be applied before other changes can be made"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:133
#, fuzzy
#| msgid "Apply"
msgid "Apply now"
msgstr "ਲਾਗੂ ਕਰੋ"

#: contents/configuration/ConfigurationShortcuts.qml:18
msgid "Shortcuts"
msgstr ""

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "ਵਾਲਪੇਪਰ"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "ਮਾਊਸ ਦੀਆਂ ਕਾਰਵਾਈਆਂ"

#: contents/configuration/MouseEventInputButton.qml:21
msgid "Input Here"
msgstr "ਇੱਥੇ ਇੰਪੁੱਟ ਕਰੋ"

#: contents/views/SdkButtons.qml:45
msgid "FormFactors"
msgstr ""

#: contents/views/SdkButtons.qml:53
msgid "Planar"
msgstr "ਪਲੇਨਰ"

#: contents/views/SdkButtons.qml:57
msgid "Vertical"
msgstr "ਵਰਟੀਕਲ"

#: contents/views/SdkButtons.qml:61
msgid "Horizontal"
msgstr "ਹਰੀਜੱਟਲ"

#: contents/views/SdkButtons.qml:65
msgid "Mediacenter"
msgstr "ਮੀਡੀਆ-ਸੈਂਟਰ"

#: contents/views/SdkButtons.qml:69
msgid "Application"
msgstr "ਐਪਲੀਕੇਸ਼ਨ"

#: contents/views/SdkButtons.qml:76
msgid "Location"
msgstr "ਟਿਕਾਣਾ"

#: contents/views/SdkButtons.qml:90
msgid "Floating"
msgstr "ਤਰਦਾ"

#: contents/views/SdkButtons.qml:94
msgid "Desktop"
msgstr "ਡੈਸਕਟਾਪ"

#: contents/views/SdkButtons.qml:98
msgid "Fullscreen"
msgstr "ਪੂਰੀ-ਸਕਰੀਨ"

#: contents/views/SdkButtons.qml:102
msgid "Top Edge"
msgstr "ਉੱਪਰੀ ਕੰਢਾ"

#: contents/views/SdkButtons.qml:106
msgid "Bottom Edge"
msgstr "ਹੇਠਲਾ ਕੰਢਾ"

#: contents/views/SdkButtons.qml:110
msgid "Left Edge"
msgstr "ਖੱਬਾ ਕੰਢਾ"

#: contents/views/SdkButtons.qml:114
msgid "Right Edge"
msgstr "ਸੱਜਾ ਕੰਢਾ"

#: contents/views/SdkButtons.qml:126
msgid "Configure Containment"
msgstr ""
